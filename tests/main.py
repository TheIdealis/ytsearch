def test_imports():
    import ytsearch
    import yaml
    import urwid
    import requests
    import pafy
    import clipboard
    import fuzzywuzzy
    return None


def test_default_files():
    from ytsearch import program
    program.check_default_files()
    return None


def test_youtube_interface():
    from ytsearch import youtube
    results = youtube.search('Miracle Of Sound')
    assert len(results) != 0
    videos, name = youtube.get_playlist('PLOl4b517qn8glTSmQfDv1KFBhsMXXWh-3')
    assert len(videos) != 0
    return None


def test_settings():
    from ytsearch import settings
    import os
    assert settings.CACHE_LOCATION != ''
    assert settings.find_keybinding('PAGE search') == '1'
    assert settings.load_settings() != {}
    assert settings.merge_settings({}, {'one': 'two'}) == {'one': 'two'}
    user_settings = settings.load_user_settings()
    settings.save_settings(user_settings)
    assert os.path.exists('{}/settings.yaml'.format(settings.CONF_DIR))
    return None


def test_videos():
    from ytsearch import ui
    videos = []
    for i in range(10):
        video = ui.Video(str(i), str(i), cache=None)
        videos.append(video)
    check = videos[0]
    assert check in videos
    assert check.name == '0'
    check.status = 'testing'
    assert check.status == 'testing'
    assert hasattr(check, 'terminal')
    assert hasattr(check, 'downloading')
    assert hasattr(check, 'widget')

    assert check.resource() is not None
    return None
